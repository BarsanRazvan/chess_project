function arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length !== b.length) return false;
  
  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}

basicStartMatrix = [ [    {type: 'RookPiece',   colour: 'BLACK'},
                          {type: 'KnightPiece', colour: 'BLACK'},
                          {type: 'BishopPiece', colour: 'BLACK'},
                          {type: 'QueenPiece',  colour: 'BLACK'},
                          {type: 'KingPiece',   colour: 'BLACK'},
                          {type: 'BishopPiece', colour: 'BLACK'},
                          {type: 'KnightPiece', colour: 'BLACK'},
                          {type: 'RookPiece',   colour: 'BLACK'},
                      ],
                      [   {type: 'PawnPiece', colour: 'BLACK'},
                          {type: 'PawnPiece', colour: 'BLACK'},
                          {type: 'PawnPiece', colour: 'BLACK'},
                          {type: 'PawnPiece', colour: 'BLACK'},
                          {type: 'PawnPiece', colour: 'BLACK'},
                          {type: 'PawnPiece', colour: 'BLACK'},
                          {type: 'PawnPiece', colour: 'BLACK'},
                          {type: 'PawnPiece', colour: 'BLACK'},
                      ],
                      ['EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
                      ['EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
                      ['EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
                      ['EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
                      [   {type: 'PawnPiece', colour: 'WHITE'},
                          {type: 'PawnPiece', colour: 'WHITE'},
                          {type: 'PawnPiece', colour: 'WHITE'},
                          {type: 'PawnPiece', colour: 'WHITE'},
                          {type: 'PawnPiece', colour: 'WHITE'},
                          {type: 'PawnPiece', colour: 'WHITE'},
                          {type: 'PawnPiece', colour: 'WHITE'},
                          {type: 'PawnPiece', colour: 'WHITE'},
                      ],
                      [   {type: 'RookPiece',   colour: 'WHITE'},
                          {type: 'KnightPiece', colour: 'WHITE'},
                          {type: 'BishopPiece', colour: 'WHITE'},
                          {type: 'QueenPiece',  colour: 'WHITE'},
                          {type: 'KingPiece',   colour: 'WHITE'},
                          {type: 'BishopPiece', colour: 'WHITE'},
                          {type: 'KnightPiece', colour: 'WHITE'},
                          {type: 'RookPiece',   colour: 'WHITE'},
                      ]
              ];

function retrieveState(){
  let currentTurn = null;
  let historyIndex = 0;
  let historyArray  = [];
  if(localStorage.getItem('initialMatrix') != null){
      try{
          let initialMatrix = JSON.parse(localStorage.getItem('initialMatrix'));
          currentTurn = localStorage.getItem('currentTurn');
          historyIndex = localStorage.getItem('historyIndex') * 1;
          historyArray = JSON.parse(localStorage.getItem('historyArray'));
          localStorage.removeItem('initialMatrix');
          localStorage.removeItem('currentTurn');
          localStorage.removeItem('historyIndex');
          localStorage.removeItem('historyArray');
          return {
                    initialMatrix: initialMatrix,
                    currentTurn: currentTurn,
                    historyIndex: historyIndex,
                    historyArray: historyArray
                 };
      }catch(SyntaxError){
          localStorage.removeItem('initialMatrix');
          console.log("ERROR : Cannot retrieve last game");
      }
  }

  return {
          initialMatrix: basicStartMatrix,
          currentTurn  : PieceColour.WHITE,
          historyIndex: historyIndex,
          historyArray: historyArray
        };
}

function getOppositeColour(colour){
  if(colour == PieceColour.BLACK){
    return PieceColour.WHITE;
  } else {
    return PieceColour.BLACK;
  }
}

function dragableHelper(event, ui){
  if(ui.helper.data('dropped')){
      let $element = $(this);
      let remaining = $element.data('pieces-left');
      if(remaining < 2){
          $element.draggable('disable');
          $element.addClass('empty-piece');
      }
      
      $element.data('pieces-left', remaining - 1);
  }
}

function createDraggablePiece(colour, symbol, id, counter){
  return $(`<li>`).data('pieces-left', counter)
                  .draggable(
                            {
                                revert : 'invalid',
                                helper : 'clone',
                                start  : (event, ui) => { 
                                  ui.helper.data('dropped', false);
                                },
                                stop   : dragableHelper
                            }
                  ).addClass('piece ' + colour)
                  .attr('id', id)
                  .text(symbol);
}

function extractTypeFromId(id){
  if(id.includes('Pawn')){
    return 'PawnPiece';
  }

  if(id.includes('King')){
    return 'KingPiece';
  }

  if(id.includes('Queen')){
    return 'QueenPiece';
  }

  if(id.includes('Rook')){
    return 'RookPiece';
  }

  if(id.includes('Bishop')){
    return 'BishopPiece';
  }

  if(id.includes('Knight')){
    return 'KnightPiece';
  }
}

function convertMultiplayerMove(move, pieces){
  let formatedMove = {
                        oldX: move.from.x * 1, 
                        oldY: move.from.y * 1, 
                        newX: move.to.x * 1, 
                        newY: move.to.y * 1,
                        turn: move.by,
                        typeTaken: null
                    };
  if(pieces[formatedMove.oldX][formatedMove.oldY] != null){
    formatedMove.type = pieces[formatedMove.oldX][formatedMove.oldY].constructor.name;
  }

  if(pieces[formatedMove.newX][formatedMove.newY] != null){
    formatedMove.typeTaken = pieces[formatedMove.newX][formatedMove.newY].constructor.name;
  }

  return formatedMove;
}

function extractMessages(newMessages, sender){
  let completed = '';

  newMessages.forEach(message => {
    completed = completed + sendeder + ': ' + message + '\n';
  });
  
  if(completed !== ''){
    return completed;
  } else {
    return null;
  }
}