class Countdown{
    constructor(duration){
        //How long the counter should be displayed for
        this.duration = duration + 1;
    }

    start(){
        //Create an element which will display the countdown
        let counterElem = document.createElement('h1');
        document.body.appendChild(counterElem);

        //Decrement the duration every 1000ms (1s)
        let timeInterval = setInterval(() => {
            //Decrement the duration
            this.duration -= 1;

            //Update the duration for the user
            counterElem.textContent = this.duration;

            // If the count down is finished, stop the counter
            //TO DO: change to do with events
            if (this.duration < 1) {
              clearInterval(timeInterval);
              document.body.removeChild(counterElem);
            }

          }, 1000);
    }
}