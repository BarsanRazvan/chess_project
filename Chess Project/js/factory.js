const registeredPiecesFactory = {};

registeredPiecesFactory['KingPiece'] = KingPiece,
registeredPiecesFactory['QueenPiece'] = QueenPiece;
registeredPiecesFactory['RookPiece'] = RookPiece;
registeredPiecesFactory['BishopPiece'] = BishopPiece;
registeredPiecesFactory['KnightPiece'] = KnightPiece;
registeredPiecesFactory['PawnPiece'] = PawnPiece;


class PieceFactory{
    constructor(){
    }

    getPiece(type, props){
        return new registeredPiecesFactory[type](props.colour, props.rowCoord, props.columnCoord); 
    }
}