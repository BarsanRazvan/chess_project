class CommandHistory{
    constructor(){
        this.clear();
    }

    push(move){
        this._history.length = this._index;
        this._history[this._index] = move;
        this._index++;
    }

    pop(){
        if(this._index > 0){
            this._index--;
            return this._history[this._index];
        }else {
            return null;
        }
    }

    restore(){
        if(this._index < this._history.length){
            return this._history[this._index++];
        } else {
            return null;
        }
    }

    clear(){
        this._history = [];
        this._index = 0;
    }

    generateAllText(){
        let moves = [];
        for(let i = 0 ; i < this._index; i++){
            moves[i] = this._toString(this._history[i]);
        }
        return moves;
    }

    getLastText(){
        if(this._index > 0){
            return this._toString(this._history[this._index - 1]);
        }
        return '';
    }

    _toString(move){
        let taken = ''
        if(move.typeTaken != null){
            taken = `and took the enemy's ${move.typeTaken}`;
        }
        return `The ${move.turn} ${move.type} moved from (${move.oldX}, ${move.oldY}) to (${move.newX}, ${move.newY}) ${taken}`; 
    }
}