class GraphicViewer{
    $turnTitle = null;
    $historyLog = null;
    $modal = null;
    $modalMessage = null;
    $chatOutput = null;

    constructor(){
        this.$cells = [[], [], [], [], [], [], [], []];
    }

    drawMenu(){
        //create the div that will act as a container for the chess table
        let $menuWrapper = $('<div>').addClass('container');

        let $menuTitle = $('<h2>').attr('id', 'menu-title')
                                  .text('Menu');
        $menuTitle.appendTo($menuWrapper);

        this.$turnTitle = $('<h3>').attr('id', 'turn-title')
                                   .text('Turn:');
        this.$turnTitle.appendTo($menuWrapper);

        let $menuContainer = $('<div>').addClass('menu-container');
        $menuContainer.appendTo($menuWrapper);

        let $singleplayerButton = $('<button>').attr('id', 'singleplayer-button')
                                           .text('Singleplayer')
                                           .click( (event) =>{
                                                    let $element = $(event.currentTarget);
                                                    $element.parent().empty();
                                                    this.drawSinglePlayer();
                                            });
        $singleplayerButton.appendTo($menuContainer);

        let $multiplayerButton = $('<button>').attr('id', 'multiplayer-button')
                                            .text('Multiplayer')
                                            .click( (event) =>{
                                                    let $element = $(event.currentTarget);
                                                    $element.parent().empty();
                                                    this.drawMultiPlayer();
                                            });
        $multiplayerButton.appendTo($menuContainer);

        $menuWrapper.prependTo($('body'));
    }

    drawSinglePlayer(){
        let $menuContainer = $('.menu-container');
        let $startButton = $('<button>').attr('id', 'start-button')
                                         .text('Start')
                                         .click( () =>{
                                                    $(window).trigger('start-game')
                                               });
        $startButton.appendTo($menuContainer);

        let $resetButton = $('<button>').attr('id', 'reset-button')
                                         .text('Reset')
                                         .click(() => {
                                                    $(window).trigger('reset-game')
                                                });
        $resetButton.appendTo($menuContainer);

        let $puzzleButton = $('<button>').attr('id', 'puzzle-button')
                                        .text('Puzzle')
                                        .click( () =>{
                                                $(window).trigger('puzzle-game')
                                            });
        $puzzleButton.appendTo($menuContainer);
    }

    drawMultiPlayer(){
        let $menuContainer = $('.menu-container');

        let $roomName = $('<input type="text">').attr({
                                                            id: 'name-input',
                                                            placeholder: 'Insert name...'
                                                      });
        $roomName.appendTo($menuContainer);

        let $createRoomButton = $('<button>').attr('id', 'room-button')
                                            .text('Create Room')
                                            .click( () =>{
                                                $(window).trigger('create-room', [$roomName.val(), $('input[name=radio-1]:checked').val()]);
                                            });
        $createRoomButton.appendTo($menuContainer);

        let $colourChoosingWrapper = $('<fieldSet>').attr('id', 'colour-field');
        $('<legend>').text('Choose a colour')
                     .appendTo($colourChoosingWrapper);

        $('<label>').text('white')
                    .attr('for', 'radio-1')
                    .appendTo($colourChoosingWrapper);                            
        $("<input>").attr({
                            name    : 'radio-1',
                            id      : 'radio-1',
                            type    : 'radio',
                            checked : true,
                            value   : PieceColour.WHITE
                          })
                    .appendTo($colourChoosingWrapper)
                    .checkboxradio();

        $('<label>').text('black')
                    .attr('for', 'radio-2')
                    .appendTo($colourChoosingWrapper);                            
        $("<input>").attr({
                            name  : 'radio-1',
                            id    : 'radio-2',
                            type  : 'radio',
                            value : PieceColour.BLACK
                         })
                    .appendTo($colourChoosingWrapper) 
                    .checkboxradio();
        $colourChoosingWrapper.appendTo($menuContainer);

        //existing games
        let $gameContainer = $('<div>').attr('id', 'games-container')
        $('<h2>').text('Existing games').appendTo($gameContainer);

        let $gameList = $('<ol>').attr('id', 'games-list')
                                 .selectable({
                                    selected: (event, ui) => { 
                                        $(ui.selected).addClass("ui-selected").siblings().removeClass("ui-selected");           
                                    } 
                                 });
        $.ajax({
            method: 'GET',
            url: `https://chess.thrive-dev.bitstoneint.com/wp-json/chess-api/game/`,
            success: (data, status) => {
                data.forEach(game => {
                    let $gameElement = $('<li>').addClass('ui-widget-content')
                                                .text(`RoomID : ${game.ID} Name: ${game.post_title}`)
                                                .attr('data-room-id', game.ID);
                    $gameElement.appendTo($gameList);                            
                });
            }
        });
        $gameList.appendTo($gameContainer);                 

        let $connectListButton = $('<button>').attr('id', 'connect-list-button')
                                          .text('Connect to selected')
                                          .click( () =>{
                                                let $selected = $('.ui-selected').first();
                                                if($selected != null){
                                                    $(window).trigger('connect-game', [$selected.attr('data-room-id') * 1, $('input[name=radio-1]:checked').val()]);
                                                }
                                          });
        $connectListButton.appendTo($gameContainer);   

        $gameContainer.appendTo($menuContainer);
                           

        let $colourLabel = $('<h3>').attr('id', 'colour-label')
                                    .hide();
        $colourLabel.appendTo($menuContainer);

        let $roomLabel = $('<h3>').attr('id', 'room-label')
                                  .hide();
        $roomLabel.appendTo($menuContainer);

        let $roomCode = $('<input type="number">').attr({
                                                        id: 'multiplayer-input',
                                                        placeholder: 'Room code...'
                                                      });
        $roomCode.appendTo($menuContainer);


        let $connectButton = $('<button>').attr('id', 'connect-button')
                                          .text('Connect')
                                          .click( () =>{
                                                $(window).trigger('connect-game', [$('#multiplayer-input').val(), $('input[name=radio-1]:checked').val()]);
                                          });
        $connectButton.appendTo($menuContainer);

        $('#undo-button').hide();
        $('#redo-button').hide();

        this._drawChat();
    }

    _drawChat(){
        let $chatContainer = $('<div>').attr('id', 'chat-container')
                                       .addClass('container chat-container');
        $('<h2>Chat</h2>').appendTo($chatContainer);

        this.$chatOutput = $('<textarea>').attr('id', 'chat-output');
        this.$chatOutput.appendTo($chatContainer);

        let $inputContainer = $('<div>').addClass('input-container');
        let $chatInput = $('<input>').attr({
                                              id: 'chat-input',
                                              type: 'text'
                                           });
        $chatInput.appendTo($inputContainer);
        let $sendButton = $('<button>').attr('id', 'send-button')
                                       .text('SEND')
                                       .on('click', () => {
                                            let $chatInput = $('#chat-input');
                                            $(window).trigger('send-chat', [$chatInput.val()]);
                                            $chatInput.val("")
                                       });
        $sendButton.appendTo($inputContainer);

        $inputContainer.appendTo($chatContainer);

        $chatContainer.insertBefore($('#joke-container'));
    }

    connectMultiPlayer(roomcode, colour){
        $('#room-button').hide();
        $('#name-input').hide();
        $('#colour-field').hide();
        $('#multiplayer-input').hide();
        $('#connect-button').hide();
        $('#games-container').hide();
        $('#colour-label').text(`My colour: ${colour}`).show();
        $('#room-label').text(`Connected to room: ${roomcode}`).show();
    }

    drawRandomJoke(){
        //create the div that will act as a container for the chess table
        let $jokeWrapper = $('<div>').addClass('container')
                                     .attr('id', 'joke-container');

        let $menuTitle = $('<h2>').attr('id', 'joke-title')
                                    .text('Wanna hear a joke?');
        $menuTitle.appendTo($jokeWrapper);

        this.$jokeButton = $('<button>').attr('id', 'joke-button')
                                       .text('DUHH!!')
                                       .on('click', () => {
                                            $.ajax({
                                                method: "GET",
                                                url: "https://sv443.net/jokeapi/v2/joke/Dark",
                                                success: (data, status) => {
                                                    $('#delivery-text').hide();
                                                    if(data.type === 'twopart'){
                                                        $('#setup-text').text(data.setup).show();
                                                        $('#delivery-button').show();
                                                        $('#delivery-text').text(data.delivery);
                                                    }else {
                                                        $('#setup-text').text(data.joke).show();
                                                        $('#delivery-button').hide();
                                                    }
                                                }
                                            });
                                        });
        this.$jokeButton.appendTo($jokeWrapper);


        let $jokeContainer = $('<div>').addClass('joke-container');
        $jokeContainer.appendTo($jokeWrapper);

        let $setupText = $('<h3>').attr('id', 'setup-text')
                                  .hide();
        $setupText.appendTo($jokeContainer);

        let $deliveryButton = $('<button>').attr('id', 'delivery-button')
                                            .text('?')
                                            .on('click', (event) =>{
                                                $('#delivery-text').show();
                                            })
                                            .hide();
        $deliveryButton.appendTo($jokeContainer);

        let $deliveryText = $('<h3>').attr('id', 'delivery-text')
                                     .hide();
        $deliveryText.appendTo($jokeContainer);

        $jokeWrapper.appendTo($('body'));
    }

    drawHistoryTab(){
        let $historyContainer = $('<div>').addClass('container')
                                          .attr('id', 'history-container');

        let $historyTitle = $('<h2>').attr('id', 'history-title')
                                      .text('History');
        $historyTitle.appendTo($historyContainer);                              

        this.$historyLog =   $('<textarea>').attr({
                                                        id: 'history-log',
                                                        readOnly: true,
                                                        resize: false
                                                     });
        this.$historyLog.appendTo($historyContainer);                              


        let $undoButton = $('<button>').attr('id', 'undo-button')
                                        .text('Undo')
                                        .click( () => {
                                                    $(window).trigger('undo-move')
                                              });
        $undoButton.appendTo($historyContainer);

        let $redoButton = $('<button>').attr('id', 'redo-button')
                                        .text('Redo')
                                        .click( () => {
                                                    $(window).trigger('redo-move')
                                              });
        $redoButton.appendTo($historyContainer);


        $historyContainer.appendTo($('body'));
    }

    showHistoryTab(){
        $('#history-container').show();
    }

    removeHistoryTab(){
        $('#history-container').hide();
    }

    _dropableHelper( event, ui ){
        let selectedPieceSymbol = ui.draggable.text();
        let selectedPieceId = ui.draggable.attr('id');
        let $cell = $(this);
        ui.helper.data('dropped', true);
        if($cell.children().length > 0){
            let $existingPiece = $cell.children().first();
            let $draggablePiece = $(`#${$existingPiece.data('piece-id')}`);
            let piecesLeft = $draggablePiece.data('pieces-left')

            if(piecesLeft < 1){
                $draggablePiece.draggable('enable');
                $draggablePiece.removeClass('empty-piece');
            }

            $draggablePiece.data('pieces-left', piecesLeft + 1);
            $cell.empty();
        }

        let colour = null;
        if(ui.draggable.hasClass('white')){
            colour = 'white';
        }else {
            colour = 'black';
        }

        $cell.append($(`<p>${selectedPieceSymbol}</p>`).data('piece-id', selectedPieceId).addClass('piece '  + colour));
    }

    drawTable(){

        let $tableContainer = $('<div/>').addClass('table-container');

        //Alternate between white and black cells
        let backgroundColor = ['white-background', 'black-background'];
        let backgroundIndex = 0;

        //Iterate 64 times: 8 rows - 8 columns
        for(let i = 0; i < 8; i++){
            //define a new row
            for(let j = 0; j < 8; j++){
                //add the div that will act as a table cell
                let $cell =  $('<div/>').attr('id', `cell-${i}-${j}`)
                                        .data({
                                                row: i,
                                                column: j
                                              })
                                        .addClass('cell ' + backgroundColor[backgroundIndex%2])
                                        .droppable({
                                            drop: this._dropableHelper
                                        });
                this.$cells[i][j] = $cell;

                $cell.appendTo($tableContainer);
                backgroundIndex++;
            }
            //this extra increment helps with alternating colours on rows
            backgroundIndex++;
        }

        $tableContainer.appendTo($('body'));
    }
    
    drawPiece(piece){
        let colour = piece.colour.toLowerCase();
        let $element = $('<p/>').addClass('chess-piece ' + colour)
                                .text(piece.graphicSymbol)
                                .css('font-size', '2rem');

        $element.appendTo(this.$cells[piece.x][piece.y]);

        this.$cells[piece.x][piece.y].addClass('active-cell');
    }

    preparePuzzleMode(){
        let $puzzleContainer = $(`<div class='container' id='puzzle-container'>`).hide();
        let $puzzleListContainer = $(`<div class='puzzle-list-container' id='puzzle-list-container'>`);
        let $puzzleWhiteList = $(`<ul class='pawn-list' id='white-pieces-list'>`);
        let $puzzleBlackList = $(`<ul class='pawn-list' id='black-pieces-list'>`);
        
        $puzzleWhiteList.append(createDraggablePiece('white', '\u2659', 'whitePawn',   8));
        $puzzleWhiteList.append(createDraggablePiece('white', '\u2654', 'whiteKing',   1));
        $puzzleWhiteList.append(createDraggablePiece('white', '\u2655', 'whiteQueen',  1));
        $puzzleWhiteList.append(createDraggablePiece('white', '\u2656', 'whiteRook',   2));
        $puzzleWhiteList.append(createDraggablePiece('white', '\u2657', 'whiteBishop', 2));
        $puzzleWhiteList.append(createDraggablePiece('white', '\u2658', 'whiteKnight', 2));

        $puzzleBlackList.append(createDraggablePiece('black', '\u265F', 'blackPawn',   8));
        $puzzleBlackList.append(createDraggablePiece('black', '\u265A', 'blackKing',   1));
        $puzzleBlackList.append(createDraggablePiece('black', '\u265B', 'blackQueen',  1));
        $puzzleBlackList.append(createDraggablePiece('black', '\u265C', 'blackRook',   2));
        $puzzleBlackList.append(createDraggablePiece('black', '\u265D', 'blackBishop', 2));
        $puzzleBlackList.append(createDraggablePiece('black', '\u265E', 'blackKnight', 2));

        $puzzleListContainer.append($puzzleWhiteList);
        $puzzleListContainer.append($puzzleBlackList);

        $puzzleContainer.append($('<h2>Remaning pieces: </h2>'));
        $puzzleContainer.append($puzzleListContainer);

        $('body').append($puzzleContainer);
    }

    showPuzzleMode(){
        $('#puzzle-container').show();
    }
    
    stopPuzzleMode(){
        $('#puzzle-container').hide();
        $('.piece').each((index, value) => {
            $(value).parent().empty(true);
        });
    }

    prepareModal(){
        this.$modal = $(`<div id='dialog-modal'>`);
        this.$modalMessage = $('<p>').text('This game is ovaah. You won/lost');
        this.$modalMessage.appendTo(this.$modal);
        this.$modal.dialog({
            title: 'Game Over',
            modal: true,
            resizable: false,
            buttons: {
              Ok: function() {
                $( this ).dialog( "close" );
              }
            },
        });
        this.$modal.dialog('close');
    }

    changeTurn(colour){
        this.$turnTitle.text('TURN: ' + colour);
    }

    removeAllPieces(){
        $('.active-cell').each((index, value) => {
            $(value).empty();
        });
    }

    removeAllHighlights(){
        $('.possible-move').each((index, value) => {
            $(value).removeClass('possible-move');
        });
        $('.possible-danger').each((index, value)=> {
            $(value).removeClass('possible-danger');
        });
        $('.selected-cell').each((index, value) => {
            $(value).removeClass('selected-cell');
        });
    }

    removePiece(x, y){
        this.$cells[x][y].empty();
        this.$cells[x][y].removeClass('active-cell');
    }

    drawPossibleMoves(piece){
        this.removeAllHighlights();

        if(piece != null){
            this.$cells[piece.x][piece.y].addClass('selected-cell');
            piece.possibleMoves.forEach(coordinates => {
                this.$cells[coordinates[0]][coordinates[1]].addClass('possible-move');
            });
        }
    }

    movePiece(coordinates){
        this.removeAllHighlights();
        this.$cells[coordinates.oldX][coordinates.oldY].removeClass('active-cell');
        this.$cells[coordinates.newX][coordinates.newY].addClass('active-cell');
        
        let $pieceElement = this.$cells[coordinates.oldX][coordinates.oldY].children().first();
        $pieceElement.appendTo(this.$cells[coordinates.newX][coordinates.newY]);
    }

    drawFooter(){
        let $footer = $('<footer>');
        let $link = $('<a href="https://bitbucket.org/BarsanRazvan/chess_project/src/master/">');
        $(`<img src='images/bitbucket-logo.png'>`).appendTo($link).addClass('animated-img');
        $link.appendTo($footer);
        $footer.appendTo($('body'));
    }

    addHistoryEntry(entry){
        this.$historyLog.val(this.$historyLog.val() + '\n' + entry);
    }

    removeHistoryEntry(){
        let newLog = this.$historyLog.val().substring(0, this.$historyLog.val().lastIndexOf("\n"));
        this.$historyLog.val(newLog);
    }

    resetHistoryLog(){
        this.$historyLog.val('');
    }

    drawPossibleDangers(piece){
        piece.possibleDangers.forEach(coordinate => {
            let x = coordinate[0];
            let y = coordinate[1];
            this.$cells[x][y].addClass('possible-danger');
        });
    }

    removePossibleDangers(piece){
        piece.possibleDangers.forEach(coordinate => {
            let x = coordinate[0];
            let y = coordinate[1];
            this.$cells[x][y].removeClass('possible-danger');
        });
    }

    warnKingAnimation(x, y){
        let start = Date.now(); 
        let alerted = false;

        let timer = setInterval(() => {
            let timePassed = Date.now() - start;

            if (timePassed >= 1000) {
                clearInterval(timer);
                this.$cells[x][y].removeClass('possible-danger');
                return;
            }

            if(!alerted){
                this.$cells[x][y].addClass('possible-danger');
                alerted = true;
            }else {
                this.$cells[x][y].removeClass('possible-danger');
                alerted = false;
            }
        }, 200);
    }

    activateWin(colour){
        this.$modalMessage.text(`The game is ovaah. ${colour} has won`);
        this.$modal.dialog('open');
    }

    getPuzzleMatrix(){
        let matrix = [];
        for(let i = 0; i < 8; i++){
            matrix[i] = [];
            for(let j = 0; j < 8; j++){
                if(this.$cells[i][j].children().length > 0){
                    let $element = this.$cells[i][j].children().first();
                    let colour = null;

                    if($element.hasClass('white')){
                        colour = PieceColour.WHITE;
                    }else {
                        colour = PieceColour.BLACK;
                    }

                    let type = extractTypeFromId($element.data('piece-id'));

                    matrix[i][j] = {
                                     type: type,
                                     colour: colour
                                   };
                }else {
                    matrix[i][j] = 'EMPTY';
                }
            }
        }
        return matrix;
    }

    addChatEntry(message){
        this.$chatOutput.val(this.$chatOutput.val() + '\n' + message);
    }
}