
//enum for colour
const PieceColour = {'BLACK': 'BLACK', 'WHITE': 'WHITE'};

//Abstract class for pieces
class AbstractPiece{
    colour = null;
    x = null;
    y = null;
    graphicSymbol = null;

    possibleMoves = [];
    //TO BE IMPLEMENTED
    possibleDangers = [];

    //Colour and element of the piece
    constructor() {
        if (this.constructor == AbstractPiece) {
          throw new Error("Abstract classes can't be instantiated.");
        }
    }

    //Accessors
    get colour(){
        return this.colour;
    }

    set colour(colour){
        this.colour = colour;
    }

    get x(){
        return this.x;
    }

    set x(x){
        this.x = x;
    }

    get y(){
        return this.y;
    }

    set y(y){
        this.y = y;
    }

    get graphicSymbol(){
        return this.graphicSymbol;
    }

    set graphicSymbol(graphicSymbol){
        this.graphicSymbol = graphicSymbol;
    }

    movePiece(newX, newY){
        let validMove = false;
        let possibleMoves = this.possibleMoves;

        for(let i = 0; i < possibleMoves.length; i++){
            if(arraysEqual(possibleMoves[i], [newX, newY])){
                validMove = true;
                break;
            }
        }

        if(validMove){
            this.x = newX;
            this.y = newY;
        }

        return validMove;
    }

    jumpTo(newX, newY){
        this.x = newX;
        this.y = newY;
    }

    checkPossibleTake(coordinates){
        let possibleTake = false;
        coordinates.forEach(coordinate => {
            if(this.possibleMoves.filter(move => arraysEqual(move, coordinate)).length > 0){
                possibleTake = true;
            }
        });
        return possibleTake;
    }

    //Methods for generating possible moves
    //Generate possible forward moves
    _forwardPossibleMoves(x, y, pieces, maximum){
        let xIndex = x;
        let maximumIndex = 0;

        while(maximumIndex < maximum && xIndex < 7){
            if(pieces[xIndex + 1][y] == null){
                this.possibleMoves.push([xIndex + 1, y]);
            }else {
                if(pieces[xIndex + 1][y].colour != this.colour){
                    this.possibleMoves.push([xIndex + 1, y]);
                    pieces[xIndex + 1][y].possibleDangers.push([this.x, this.y]);
                }
                break;
            }
            maximumIndex++;
            xIndex++;
        } 
    }


    //Generate possible backward moves
    _backwardPossibleMoves(x, y, pieces, maximum){
        let xIndex = x;
        let maximumIndex = 0;

        while(maximumIndex < maximum  && xIndex > 0){
            if(pieces[xIndex - 1][y] == null){
                this.possibleMoves.push([xIndex - 1, y]);
            }else {
                if(pieces[xIndex - 1][y].colour != this.colour){
                    this.possibleMoves.push([xIndex - 1, y]);
                    pieces[xIndex - 1][y].possibleDangers.push([this.x, this.y]);
                }
                break;
            }
            maximumIndex++;
            xIndex--;
        }
    }

    //Generate possible left moves
    _leftPossibleMoves(x, y, pieces, maximum){
        //LEFT
        let maximumIndex = 0;
        let yIndex = y;
        while(yIndex > 0 && maximumIndex < maximum){
            if(pieces[x][yIndex - 1] == null){
                this.possibleMoves.push([x, yIndex - 1]);
            } else {
                if(pieces[x][yIndex - 1].colour != this.colour){
                    this.possibleMoves.push([x, yIndex - 1]);
                    pieces[x][yIndex - 1].possibleDangers.push([this.x, this.y]);
                }
                break;
            }
            maximumIndex++;
            yIndex--;
        }
    }

    //Generate possible right moves
    _rightPossibleMoves(x, y, pieces, maximum){
        //RIGHT
        let rightMax = 0;
        let yIndex = y;
        let maximumIndex = 0;
        while(yIndex < 7 && maximumIndex < maximum){
            if(pieces[x][yIndex + 1] == null){
                this.possibleMoves.push([x, yIndex + 1]);
            } else {
                if(pieces[x][yIndex + 1].colour != this.colour){
                    this.possibleMoves.push([x, yIndex + 1]);
                    pieces[x][yIndex + 1].possibleDangers.push([this.x, this.y]);
                }
                break;
            }
            maximumIndex++;
            yIndex++;
        }
    }

    //Generate possible diagonal moves
    _diagonalPossibleMoves(x, y, pieces, maximum){
        let diagonalUpRightMax   = 0;
        let diagonalUpLeftMax    = 0;
        let diagonalDownRightMax = 0;
        let diagonalDownLeftMax  = 0;

        let diagonalUpRightFound   = false;
        let diagonalUpLeftFound    = false;
        let diagonalDownRightFound = false;
        let diagonalDownLeftFound  = false;


        let maximumIndex = 0;

        while(maximumIndex < maximum){
            //check down diagonal
            if(x - maximumIndex > 0){
                if(!diagonalDownRightFound && y + maximumIndex < 7){
                    if(pieces[x - maximumIndex - 1][y + maximumIndex + 1] == null){
                        this.possibleMoves.push([x - maximumIndex - 1, y + maximumIndex + 1]);
                    } else {
                        diagonalDownRightFound = true;
                        if(pieces[x - maximumIndex - 1][y + maximumIndex + 1].colour != this.colour){
                            this.possibleMoves.push([x - maximumIndex - 1, y + maximumIndex + 1]);
                            pieces[x - maximumIndex - 1][y + maximumIndex + 1].possibleDangers.push([this.x, this.y]);
                        }
                    }
                }

                
                if(!diagonalDownLeftFound && y - maximumIndex > 0){
                    if(pieces[x - maximumIndex - 1][y - maximumIndex - 1] == null){
                        this.possibleMoves.push([x - maximumIndex - 1, y - maximumIndex - 1]);
                    } else {
                        diagonalDownLeftFound = true;
                        if(pieces[x - maximumIndex - 1][y - maximumIndex - 1].colour != this.colour){
                            this.possibleMoves.push([x - maximumIndex - 1, y - maximumIndex - 1]);
                            pieces[x - maximumIndex - 1][y - maximumIndex - 1].possibleDangers.push([this.x, this.y]);
                        }
                    }
                }
            }

            //check up diagonal
            if(x + maximumIndex < 7){
                if(!diagonalUpRightFound && y + maximumIndex < 7){
                    if(pieces[x + maximumIndex + 1][y + maximumIndex + 1] == null){
                        this.possibleMoves.push([x + maximumIndex + 1,y + maximumIndex + 1]);
                    } else {
                        diagonalUpRightFound = true;
                        if(pieces[x + maximumIndex + 1][y + maximumIndex + 1].colour != this.colour){
                            this.possibleMoves.push([x + maximumIndex + 1,y + maximumIndex + 1]);
                            pieces[x + maximumIndex + 1][y + maximumIndex + 1].possibleDangers.push([this.x, this.y]);
                        }
                    }
                }
                
                if(!diagonalUpLeftFound && y - maximumIndex > 0){
                    if(pieces[x + maximumIndex + 1][y - maximumIndex - 1] == null){
                        this.possibleMoves.push([x + maximumIndex + 1, y - maximumIndex - 1]);
                    } else {
                        diagonalUpLeftFound = true;
                        if(pieces[x + maximumIndex + 1][y - maximumIndex - 1].colour != this.colour){
                            this.possibleMoves.push([x + maximumIndex + 1, y - maximumIndex - 1]);
                            pieces[x + maximumIndex + 1][y - maximumIndex - 1].possibleDangers.push([this.x, this.y]);
                        }
                    }
                }
            }

            maximumIndex++;
        }
    }

    _clearDanger(){
        this.possibleDangers = [];
    }

    //Abstract method: will compute internally all possible moves
    computePossibleMoves(){
        throw new Error("Method 'move()' must be implemented.");
    }
}

class PawnPiece extends AbstractPiece{
    blackSymbol = '\u265F';
    whiteSymbol = '\u2659';
    promotion = false;
    firstMove = null;
    possibleTakes = [];

    constructor(colour, rowCoord, columnCoord) {
        super();
        super.colour = colour;
        super.x = rowCoord;
        super.y = columnCoord;
        if(colour == PieceColour.BLACK){
            super.graphicSymbol = this.blackSymbol;
            if(rowCoord == 1){
                this.firstMove = true;
            } else{ 
                this.firstMove = false;
            }
        }else {
            super.graphicSymbol = this.whiteSymbol;
            if(rowCoord == 6){
                this.firstMove = true;
            } else{ 
                this.firstMove = false;
            }
        }

    }

    movePiece(newX, newY){
        let validMove = super.movePiece(newX, newY);

        if(validMove){
            this.firstMove = false;
        }

        return validMove;
    }

    jumpTo(newX, newY){
        super.jumpTo(newX, newY);
        if(this.colour == PieceColour.WHITE){
            this.firstMove = this.x === 6;
        } else {
            this.firstMove = this.x === 1;
        }
    }

    computePossibleMoves(pieces){
        //reset possbileMoves
        this.possibleMoves = [];
        this.possibleTakes = [];

        //get the coordinates of the piece
        let x = super.x;
        let y = super.y;

        //based on colour, set the direction of the table (7 = bottom (for black) 0 = bottom (for white))
        let maxRow = null, direction = null;

        if(super.colour == PieceColour.BLACK){
            maxRow = 7;
            direction = 1;
        }else {
            maxRow = 0;
            direction = -1;;
        }

        let repeatIndex = 0;
        let increment = 0;
        let repeatLimit = this.firstMove ? 2 : 1;

        while(repeatIndex < repeatLimit){
            //If we don't have any piece infront of us, we can move forward by 1 cell 
            //and we will not take over any other cell
            if(x != maxRow && pieces[x + direction + increment][y] == null){
                this.possibleMoves.push([x + direction + increment, y]);
            } else {
                break;
            }
            repeatIndex++;
            increment = direction;
        }
        
        //If we reached the end of the table, we can promote our pawn
        if(x == maxRow){
            this.promotion = true;
        } 

        //Move in a diagonal direction to overtake another piece if it exists
        if(x != maxRow && pieces[x + direction][y + 1] != null && pieces[x + direction][y + 1].colour != this.colour){
            this.possibleMoves.push([x + direction, y + 1]);
            pieces[x + direction][y + 1].possibleDangers.push([this.x, this.y]);
        }

        if(x != maxRow && pieces[x + direction][y - 1] != null && pieces[x + direction][y - 1].colour != this.colour){
            this.possibleMoves.push([x + direction, y - 1]);
            pieces[x + direction][y - 1].possibleDangers.push([this.x, this.y]);
        }

        this.possibleTakes.push([x + direction, y + 1]);
        this.possibleTakes.push([x + direction, y - 1]);
    }
}

class KingPiece extends AbstractPiece{
    blackSymbol = '\u265A';
    whiteSymbol = '\u2654';
    constructor(colour, rowCoord, columnCoord) {
        super();
        super.colour = colour;
        super.x = rowCoord;
        super.y = columnCoord;
        if(colour == PieceColour.BLACK){
            super.graphicSymbol = this.blackSymbol;
        }else {
            super.graphicSymbol = this.whiteSymbol;
        }
    }

    computePossibleMoves(pieces){
        this.possibleMoves = [];
        //get the coordinates of the piece
        let x = super.x;
        let y = super.y;

        super._forwardPossibleMoves(x, y, pieces, 1);
        super._backwardPossibleMoves(x, y, pieces, 1);
        super._leftPossibleMoves(x, y, pieces, 1);
        super._rightPossibleMoves(x, y, pieces, 1);
        super._diagonalPossibleMoves(x, y, pieces, 1);

        pieces.forEach(row => {
            row.filter(piece => piece != null && piece.possibleMoves.length > 0 && piece.colour != this.colour).forEach(piece =>{
                if(piece.constructor.name === 'PawnPiece'){
                    this.possibleMoves.forEach((possibleMove, index) => {
                        if(piece.possibleTakes.filter(possibleDanger => arraysEqual(possibleDanger, possibleMove)).length > 0){
                            this.possibleMoves.splice(index, 1);
                        }
                    });
                } else {
                    this.possibleMoves.forEach((possibleMove, index) => {
                        if(piece.possibleMoves.filter(possibleDanger => arraysEqual(possibleDanger, possibleMove)).length > 0){
                            this.possibleMoves.splice(index, 1);
                        }
                    });
                }
            }); 
        });
    }

}

class QueenPiece extends AbstractPiece{
    blackSymbol= '\u265B';
    whiteSymbol= '\u2655';

    constructor(colour, rowCoord, columnCoord) {
        super();
        super.colour = colour;
        super.x = rowCoord;
        super.y = columnCoord;
        if(colour == PieceColour.BLACK){
            super.graphicSymbol = this.blackSymbol;
        }else {
            super.graphicSymbol = this.whiteSymbol;
        }
    }

    computePossibleMoves(pieces){
        this.possibleMoves = [];

        //get the coordinates of the piece
        let x = super.x;
        let y = super.y

        super._forwardPossibleMoves(x, y, pieces, 7);
        super._backwardPossibleMoves(x, y, pieces, 7);
        super._leftPossibleMoves(x, y, pieces, 7);
        super._rightPossibleMoves(x, y, pieces, 7);
        super._diagonalPossibleMoves(x, y, pieces, 7);
    }
}

class RookPiece extends AbstractPiece{
    blackSymbol = '\u265C';
    whiteSymbol =  '\u2656';

    constructor(colour, rowCoord, columnCoord) {
        super();
        super.colour = colour;
        super.x = rowCoord;
        super.y = columnCoord;
        if(colour == PieceColour.BLACK){
            super.graphicSymbol = this.blackSymbol;
        }else {
            super.graphicSymbol = this.whiteSymbol;
        }
    }

    computePossibleMoves(pieces){
        this.possibleMoves = [];
        //get the coordinates of the piece
        let x = super.x;
        let y = super.y;

        super._forwardPossibleMoves(x, y, pieces, 7);
        super._backwardPossibleMoves(x, y, pieces, 7);
        super._leftPossibleMoves(x, y, pieces, 7);
        super._rightPossibleMoves(x, y, pieces, 7);
    }
}

class BishopPiece extends AbstractPiece{
    blackSymbol = '\u265D';
    whiteSymbol = '\u2657';

    constructor(colour, rowCoord, columnCoord) {
        super();
        super.colour = colour;
        super.x = rowCoord;
        super.y = columnCoord;
        if(colour == PieceColour.BLACK){
            super.graphicSymbol = this.blackSymbol;
        }else {
            super.graphicSymbol = this.whiteSymbol;
        }
    }

    computePossibleMoves(pieces){
        this.possibleMoves = [];
        //get the coordinates of the piece
        let x = super.x;
        let y = super.y;

        super._diagonalPossibleMoves(x, y, pieces, 7);
    }
}

class KnightPiece extends AbstractPiece{
    blackSymbol = '\u265E';
    whiteSymbol = '\u2658';

    constructor(colour, rowCoord, columnCoord) {
        super();
        super.colour = colour;
        super.x = rowCoord;
        super.y = columnCoord;
        if(colour == PieceColour.BLACK){
            super.graphicSymbol = this.blackSymbol;
        }else {
            super.graphicSymbol = this.whiteSymbol;
        }
    }

    computePossibleMoves(pieces){
        //get the coordinates of the piece
        let x = super.x;
        let y = super.y;

        this.possibleMoves = [];

        //possible knight increment pairs
        let incrementX = [-2, -1, 1, 2, -2, -1,  1,  2];
        let incrementY = [ 1,  2, 2, 1, -1, -2, -2, -1];

        for(let i = 0; i < 8; i++){
            let currentX = x + incrementX[i];
            let currentY = y + incrementY[i];
            if(currentX <= 7 && currentX >= 0 && currentY <= 7 && currentY >= 0){
                if(pieces[currentX][currentY] == null){
                    this.possibleMoves.push([currentX, currentY]);
                } else {
                    if(pieces[currentX][currentY].colour != this.colour){
                        this.possibleMoves.push([currentX, currentY]);
                        pieces[currentX][currentY].possibleDangers.push([this.x, this.y]);
                    }
                }
            }
        }
    }
}