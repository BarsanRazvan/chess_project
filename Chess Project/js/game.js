/****************************************************************************************************************************
 *  Author: Barsan Razvan
 *   .--.              .--.
 *  : (\ ". _......_ ." /) :
 *  '.    `        `    .'
 *    /'   _        _   `\
 *   /     0}      {0     \
 *  |       /      \       |   <- Here to judge my code
 *  |     /'        `\     |
 *   \   | .  .==.  . |   /
 *    '._ \.' \__/ './ _.'
 *    /  ``'._-''-_.'``  \
 *
 *   possible improvements:
 *    -separate game in 2 classes: base class for single player mode and child class for multiplayer mode
 *    -maybe clean up the logic behind pieces
 *    -alternative to inheritance: move methods in utility classes 
 *    -prbly a good idea to mimic the MVC arch.
 *    -use webpack
 *    -reorder the methods - group them based on logic
 * 
 *    TO DO(might not make it until final push):
 *    - document the code(add comments inside methods)
 *    - complete the logic behind king piece
 *    - test for more hidden bugs (they are always there, watching)
 *    - rename certain variables
 *    - add restriction for puzzle so we cannot start the match without a king
 *    - add a way to remove a piece from the board in puzzle mode
 ****************************************************************************************************************************/

class Game{
    _selectedCoordinates = null;
    _currentTurn = null;
    _initialMatrix = null;
    _whiteKingCoords = null;
    _blackKingCoords = null;
    _started = false;
    _puzzleEnabled = false;
    _history = null;
    _gameMode = null;
    _roomCode = null;
    _myColour = null;
    _messageId = null;

    constructor(){
        this._graphicalDrawer = new GraphicViewer();
        this._pieces = [[], [], [], [], [], [] ,[] , []];
        this._pieceFactory = new PieceFactory();
        this._history = new CommandHistory();
    }

    //Generate pieces from the initial matrix by using a factory class
    _addPieces(){
        let piecesMatrix = this._initialMatrix;
        for(let i = 0; i < 8; i ++){
            for(let j = 0; j < 8; j ++){
                //If the initial matrix contains a piece on the (i,j) position, create an object
                //with those props and tell the graphical designer class to draw it on the table
                //Otherwise, mark it as empty(null)
                if(piecesMatrix[i][j] !== 'EMPTY'){
                    let type = piecesMatrix[i][j].type;
                    let pieceColour = null;

                    //Select the colour and if it's the case, save the king's coordinates (for checkmate validation)
                    if(piecesMatrix[i][j].colour === 'BLACK'){
                        pieceColour = PieceColour.BLACK;
                        if(type === 'KingPiece'){
                            this._blackKingCoords = {
                                                    x: i,
                                                    y : j
                                                };
                        }
                    }else {
                        pieceColour = PieceColour.WHITE;
                        if(type === 'KingPiece'){
                            this._whiteKingCoords = {
                                                    x: i,
                                                    y : j
                                                };
                        }
                    }

                    this._pieces[i][j] = this._pieceFactory.getPiece(type, {rowCoord: i,
                                                                            columnCoord: j,
                                                                            colour: pieceColour
                                                                            }); 
                    this._graphicalDrawer.drawPiece(this._pieces[i][j]);
                } else {
                    this._pieces[i][j] = null;
                }
            }   
        }
    }

    //Add events to each individual cell of the board
    _addCellsEvents(){
        this._graphicalDrawer.$cells.forEach(row => {
            row.forEach($value => {
                $value.on('click', () =>{
                    if(this._gameMode === 'MULTIPLAYER'){
                        if(this._currentTurn != this._myColour){
                            return;
                        }
                    } 

                    if(this._started){
                        let x = $value.data('row');
                        let y = $value.data('column');
                        this._checkInteraction(x, y);
                    }
                 });
            });
        });
    }

    //Method for gettting the king of the current colour(based on turns)
    _getActiveKing(){
        let kingCoords = null;
        if(this._currentTurn == PieceColour.WHITE){
            kingCoords = this._whiteKingCoords;
        }else {
            kingCoords = this._blackKingCoords;
        }

        return kingCoords;
    }

    //Check if we can move any piece (if the king is in danger we cannot move unless we get the king out of danger)
    _checkMoveKing(x, y){
        let kingCoords = this._getActiveKing();
        if(this._pieces[kingCoords.x][kingCoords.y].possibleDangers.length > 0){
            if(this._pieces[x][y].constructor.name === 'KingPiece'){
                return true;
            } else {
                if(this._pieces[x][y].checkPossibleTake(this._pieces[kingCoords.x][kingCoords.y].possibleDangers)){
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return true;
        }
    }

    //Update the coordinates of the current king
    _changeKingCoords(x, y){
        let newCoords = {
                        x: x,
                        y: y

                    }
        if(this._currentTurn == PieceColour.WHITE){
            this._whiteKingCoords = newCoords;
        }else {
            this._blackKingCoords = newCoords;
        }
    }

    //Check if the king is in check mate
    _checkWin(){
        let kingCoords = this._getActiveKing();
        let win = true;
        if(this._pieces[kingCoords.x][kingCoords.y].possibleDangers.length > 0 && (this._pieces[kingCoords.x][kingCoords.y].possibleMoves.length === 0)){
            this._pieces.forEach(row => {
                row.filter(piece => piece != null && piece.colour == this._currentTurn && piece.possibleMoves.length > 0).forEach(piece => {
                    if(piece.checkPossibleTake(this._pieces[kingCoords.x][kingCoords.y].possibleDangers)){
                        win = false;
                    }
                });
            });
        } else {
            win = false;
        }

        if(win){
            this._graphicalDrawer.activateWin(getOppositeColour(this._currentTurn));
            this._started = false;
        }
    }

    //Logic for interacting with cells: if we need to move a piece, if we need to select or deselect a piece
    _checkInteraction(x, y){
        let pieces = this._pieces;
        let selectedCoord = this._selectedCoordinates;

        if(pieces[x][y] != null){
            if(selectedCoord != null){
                let oldX = selectedCoord.xCoord;
                let oldY = selectedCoord.yCoord;
                this._movePiece(x, y);
            } else { 
                if(this._checkTurn(x, y)){
                    if(this._checkMoveKing(x, y)){
                        this._selectedCoordinates = {xCoord : x, yCoord : y};
                        this._graphicalDrawer.drawPossibleMoves(pieces[x][y]);
                    } else {
                        let kingCoord = this._getActiveKing();
                        this._graphicalDrawer.warnKingAnimation(kingCoord.x, kingCoord.y);
                    }
                }
            }

        } else {
            if(selectedCoord != null){
                this._movePiece(x, y);
            }
        }
    }

    //Remove a piece from (x,y) from both the backend matrix and the view
    _removePiece(x, y){
        this._pieces[x][y] = null;
        this._graphicalDrawer.removePiece(x, y);
    }

    //Check if we can interact with a piece: we can only interact with it if it's 
    _checkTurn(x, y){
        let pieces = this._pieces;
        if(pieces[x][y].colour == this._currentTurn){
            return true;
        } else {
            return false;
        }
    }

    //Check if we can move a piece and if it is a valid move, move the piece and update the view
    _movePiece(newX, newY){
        let oldX = this._selectedCoordinates.xCoord;
        let oldY = this._selectedCoordinates.yCoord;

        let validMove = this._pieces[oldX][oldY].movePiece(newX, newY);
        let pieces = this._pieces;

        if(validMove){
            let piece = pieces[oldX][oldY];

            if(piece.constructor.name == 'KingPiece'){
                this._changeKingCoords(newX, newY);
            }

            let move =  {    
                            oldX : oldX,
                            oldY : oldY,
                            newX : piece.x,
                            newY : piece.y,
                            turn : this._currentTurn,
                            type : piece.constructor.name,
                            typeTaken : null
                        }

            if(pieces[newX][newY] != null && pieces[oldX][oldY].colour != pieces[newX][newY].colour){
                move.typeTaken = pieces[newX][newY].constructor.name;
                this._removePiece(newX, newY);
            }

            pieces[oldX][oldY] = null;
            pieces[newX][newY] = piece;

            this._graphicalDrawer.movePiece(move);
            this._history.push(move);
            this._graphicalDrawer.addHistoryEntry(this._history.getLastText());

            this._clearAllPossibleDangers();
            if(this._gameMode === 'MULTIPLAYER'){
                this._sendMultiplayerMove(move);
            } else {
                this._changeTurn();
            }
            this._computeAllPossibleMoves();
            this._checkWin();
        } else {
            this._graphicalDrawer.removeAllHighlights();
        }

        this._selectedCoordinates = null;
    }

    //Send the move to the API
    _sendMultiplayerMove(move){
        $.ajax({
            method: 'POST',
            url: `https://chess.thrive-dev.bitstoneint.com/wp-json/chess-api/game/${this._roomCode}`,
            data: { move : {from: {
                                    x: move.oldX,
                                    y: move.oldY
                                 },
                            to: {
                                    x: move.newX,
                                    y: move.newY
                                },
                            by: this._myColour,
                   }
                 },
            success: () => {this._changeTurn();}
        });
    }

    //Change the turn: if it's white change it to black and if it's black change it to white
    _changeTurn(){
        if(this._currentTurn == PieceColour.WHITE){
            this._currentTurn = PieceColour.BLACK;
            this._graphicalDrawer.changeTurn('BLACK');
        } else {
            this._currentTurn = PieceColour.WHITE;
            this._graphicalDrawer.changeTurn('WHITE');
        }
    }

    //For each existing piece, update the possible moves it can make: the king will be last so we will not let him move in danger's way 
    _computeAllPossibleMoves(){
        this._pieces.forEach(row => {
            row.filter(piece => piece != null).forEach(piece => {
                piece.computePossibleMoves(this._pieces);
            });
        });
        let kingCoord = this._getActiveKing();
        let king = this._pieces[kingCoord.x][kingCoord.y]
        king.computePossibleMoves(this._pieces);
    }

    //Reset all calculated dangers
    _clearAllPossibleDangers(){
        this._pieces.forEach(row => {
            row.filter(piece => piece != null).forEach(piece => {
                piece._clearDanger();
            });
        });
    }

    //From the object matrix generate a matrix that will be stored in localstorage and from which we will be able to generate the object matrix again 
    //on revisiting
    _generateRetrievalMatrix(){
        let retrievalMatrix = [[], [], [], [], [], [] ,[] , []];;

        for(let i = 0; i < 8; i++){
            for(let j = 0; j < 8; j++){
                if(this._pieces[i][j] != null){
                    retrievalMatrix[i][j] = {   
                                                type  : this._pieces[i][j].constructor.name,
                                                colour: this._pieces[i][j].colour
                                            };
                } else {
                    retrievalMatrix[i][j] = 'EMPTY';
                }
            }
        }

        return retrievalMatrix;
    }

    //Restore the game to the normal start of the game(flags, positions and the view)
    _restartGame(){
        this._graphicalDrawer.removeAllHighlights();
        this._graphicalDrawer.removeAllPieces();
        this._graphicalDrawer.resetHistoryLog();
        this._selectedCoordinates = null;
        this._pieces = [[], [], [], [], [], [] ,[] , []];
        this._initialMatrix = basicStartMatrix;
        this._currentTurn = PieceColour.WHITE;
        this._history.clear();
        this._startGame();
    }

    //Set all the necessary flags and make it possible for the user to move pieces
    _startGame(){
        if(this._puzzleEnabled){
            this._updatePuzzleMatrix();
            this._graphicalDrawer.stopPuzzleMode();
            this._puzzleEnabled = false;
            this._graphicalDrawer.showHistoryTab();
        } else {
            this._retrieveHistoryLog();
        }
        this._graphicalDrawer.changeTurn(this._currentTurn);
        this._addPieces();
        this._clearAllPossibleDangers();
        this._computeAllPossibleMoves();
    }

    //Used for making a move that does not require validation: for undo, redo and for making the opponent's move in multiplayer mode
    _invalidatedMove(move){
        this._pieces[move.newX][move.newY] = this._pieces[move.oldX][move.oldY];
        this._pieces[move.newX][move.newY].jumpTo(move.newX, move.newY);
        this._pieces[move.oldX][move.oldY] = null;
        if(this._pieces[move.newX][move.newY].constructor.name === 'KingPiece'){
            this._changeKingCoords(move.newX, move.newY);
        }
        this._clearAllPossibleDangers();
        this._computeAllPossibleMoves();
        this._graphicalDrawer.movePiece(move);
    }

    //Undo the last move
    _undo(){
        let move = this._history.pop();
        if (move != null){
            this._currentTurn = move.turn;  
            this._invalidatedMove({ 
                                oldX : move.newX,
                                oldY : move.newY,
                                newX : move.oldX,
                                newY : move.oldY,
                                turn : move.turn
                              });  

            this._graphicalDrawer.changeTurn(move.turn);      
            this._graphicalDrawer.removeHistoryEntry();
            if(move.typeTaken != null){
                this._pieces[move.newX][move.newY] = this._pieceFactory.getPiece(move.typeTaken, {  rowCoord: move.newX,
                                                                                                    columnCoord: move.newY,
                                                                                                    colour: getOppositeColour(move.turn)
                                                                                                 }); 
                this._graphicalDrawer.drawPiece(this._pieces[move.newX][move.newY]);                                                                                            
            }
        } 
    }

    //Redo the latest move
    _redo(){
        let move = this._history.restore();
        if (move != null){
            if(move.typeTaken != null){
                this._removePiece(move.newX, move.newY);                                                                                       
            }
            this._invalidatedMove(move);    
            this._currentTurn = getOppositeColour(move.turn);
            this._graphicalDrawer.changeTurn(this._currentTurn);      
            this._graphicalDrawer.addHistoryEntry(this._history.getLastText());
        } 
    }

    //From the history object, get the history log and add it to the view
    _retrieveHistoryLog(){
        if(this._history._index > 0){
            let log = this._history.generateAllText();
            log.forEach((entry) => {
                this._graphicalDrawer.addHistoryEntry(entry);
            });
        }
    }

    //Set the initial state of the board to the state of the board (necessary in puzzle mode)
    _updatePuzzleMatrix(){
        let matrix = this._graphicalDrawer.getPuzzleMatrix();
        this._initialMatrix = matrix;
    }

    //Add all windows events: for start game, restart game, etc
    _addWindowEvents(){
        $(window).on('start-game', () => {
            if(!this._started){
                this._startSinglePlayer();
            }
            event.stopPropagation()
        });

        $(window).on('reset-game', () => {
            if(this._started){
                this._restartGame();
            }
            event.stopPropagation()
        });

        $(window).on('puzzle-game', () => {
            if(!this._started){
                this._puzzleMode();
            }
            event.stopPropagation()
        });

        $(window).on('beforeunload', () => {
            if(this._started && this._gameMode !== 'MULTIPLAYER'){
                localStorage.setItem('initialMatrix', JSON.stringify(this._generateRetrievalMatrix()));
                localStorage.setItem('historyIndex', this._history._index);
                localStorage.setItem('historyArray', JSON.stringify(this._history._history));
                localStorage.setItem('currentTurn', this._currentTurn);
            }
            event.stopPropagation()
        });

        $(window).on('undo-move', () => {
            if(this._started){
                this._undo();
            }
            event.stopPropagation()
        });

        $(window).on('redo-move', () => {
            if(this._started){
                this._redo();
            }
            event.stopPropagation()
        });

        $(window).on('keydown', (event) =>{
            if(event.keyCode === 68 && this._selectedCoordinates != null){
                let x = this._selectedCoordinates.xCoord;
                let y = this._selectedCoordinates.yCoord;
                this._graphicalDrawer.drawPossibleDangers(this._pieces[x][y]);
            }
         });

        $(window).on('keyup', (event) =>{
        if(event.keyCode === 68 && this._selectedCoordinates != null){
            let x = this._selectedCoordinates.xCoord;
            let y = this._selectedCoordinates.yCoord;
            this._graphicalDrawer.removePossibleDangers(this._pieces[x][y]);
        }
        });

        $(window).on('create-room', (event, roomName, colour) =>{
            let name = roomName === '' ? 'Barsanitor' : roomName;
            $.ajax({
                method: 'POST',
                url: 'https://chess.thrive-dev.bitstoneint.com/wp-json/chess-api/game',
                data: { name: name },
                success: (data, status) => {
                    this._startMultiplayer(data.ID, colour);
                }
            });
        });

        $(window).on('connect-game', (event, roomcode, colour) =>{
            if(roomcode != ''){
                $.ajax({
                    method: 'GET',
                    url: `https://chess.thrive-dev.bitstoneint.com/wp-json/chess-api/game/${roomcode}`,
                    success: (data, status) => {
                        this._graphicalDrawer.connectMultiPlayer(data.ID);
                        this._startMultiplayer(data.ID, colour);
                        this._checkExistingMoves(data.moves);
                    },
                    error: (data, status) => {}
                });
            }
        });

        $(window).on('send-chat', (event, message) =>{
            if(message.trim() != ''){
                let formatedMessage = this._myColour + ' : ' + message;
                this._messageId++;
                this._graphicalDrawer.addChatEntry(formatedMessage);
                $.ajax({
                    method: 'POST',
                    url: `https://chess.thrive-dev.bitstoneint.com/wp-json/chess-api/game/${this._roomCode}`,
                    data: { move : {
                                        message: formatedMessage,
                                        messageId : this._messageId
                                   }
                         },
                    success: () => {this._changeTurn();}
                });
            }
        });
    }

    //Do all the necessary things for updating the opponent's move (view and history log included)
    _updateOpponentMove(move){
        let formatedMove = convertMultiplayerMove(move, this._pieces);

        if(formatedMove.typeTaken){
            this._graphicalDrawer.removePiece(formatedMove.newX, formatedMove.newY);
        }

        this._history.push(formatedMove);
        this._graphicalDrawer.addHistoryEntry(this._history.getLastText());
        this._invalidatedMove(formatedMove);
        this._changeTurn();
    }

    //Start the game in single player mode(offline)
    _startSinglePlayer(){
        let retrievedState = retrieveState();
        this._initialMatrix = retrievedState.initialMatrix;
        this._currentTurn = retrievedState.currentTurn;
        this._history._index = retrievedState.historyIndex;
        this._history._history = retrievedState.historyArray;
        this._gameMode = 'SINGLEPLAYER';
        this._started = true;
        this._startGame();
    }

    //start the game in multiplayer mode
    _startMultiplayer(id, colour){
        this._roomCode = id;
        this._myColour = colour;
        this._gameMode = 'MULTIPLAYER';
        this._started = true;
        this._graphicalDrawer.connectMultiPlayer(id, colour);
        let $sending = false;
        setInterval(() => {
            $.ajax({
                method: 'GET',
                url: `https://chess.thrive-dev.bitstoneint.com/wp-json/chess-api/game/${this._roomCode}`,
                async: false,
                success: (data) => {
                        let move = data.moves[data.moves.length - 1];
                        if(typeof move !== 'undefined'){
                            if(typeof move.message !== 'undefined'){
                                if(move.messageId != this._messageId){
                                    this._messageId = move.messageId;
                                    this._graphicalDrawer.addChatEntry(data.moves[data.moves.length - 1].message);
                                }
                            }else {
                                if(this._currentTurn != this._myColour && data.moves.length > 0) {
                                    if(typeof move.by === 'undefined' || move.by.toUpperCase() !== this._myColour){
                                        try{
                                            this._updateOpponentMove(move);
                                        }catch(TypeError){
                                            console.log(move);
                                        }
                                    }
                                }
                            }
                        }
                },
                error: (data) => {}
            });
        }, 1000);
        this._restartGame();
    }

    //check if the api's moves has been updated
    _checkExistingMoves(moves){
        if(moves.length > 0){
            let validMoves = moves.filter(move => typeof move.message === 'undefined');
            validMoves.forEach(move => {
                this._updateOpponentMove(move);
            });
            if(validMoves.length % 2 === 0){
                this._currentTurn = PieceColour.BLACK;
            } else {
                this._currentTurn = PieceColour.WHITE;
            }
        }
    }

    //Prepare the initial state of the game: add the necessary view elements and set teh necessary flags
    prepareGame(){
        this._graphicalDrawer.drawMenu();
        this._graphicalDrawer.drawTable();
        this._graphicalDrawer.drawHistoryTab();
        this._graphicalDrawer.prepareModal();
        this._graphicalDrawer.preparePuzzleMode();
        this._graphicalDrawer.drawRandomJoke();
        this._graphicalDrawer.drawFooter();
        this._messageId = 0;
        this._addCellsEvents();
        this._addWindowEvents();
    }

    //Activate puzzle mode
    _puzzleMode(){
        this._graphicalDrawer.removeHistoryTab();
        this._graphicalDrawer.showPuzzleMode();
        this._puzzleEnabled = true;
    }
}